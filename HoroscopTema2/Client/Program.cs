﻿using Grpc.Core;
using System;
using System.Globalization;
using System.IO;
using System.Threading;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 5001;
            int OK = 1;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

            do
            {
                Console.WriteLine("Please enter the date (MM/dd/yyyy): ");
                string data = Console.ReadLine();

                //VALIDATION
                do
                {
                    foreach (char ch in data)
                    {
                        OK = 1;
                        if ((ch < '0' || ch > '9') && ch != '/')
                        {
                            OK = 0;
                            Console.WriteLine("Invalid date. Try enter another date.");
                            data = Console.ReadLine();
                            OK = 0;
                        }
                        else
                        { try
                            {
                                string[] dateParts = data.Split('/');

                                DateTime testDate = new
                                    DateTime(Convert.ToInt32(dateParts[2]),
                                    Convert.ToInt32(dateParts[0]),
                                    Convert.ToInt32(dateParts[1]));
                            }
                            catch
                            {
                                Console.WriteLine("Invalid date. Try enter another date (MM/dd/yyyy) : ");
                                data = Console.ReadLine();
                                OK = 0;
                            }
                        }              
                    }
                } while (OK == 0);


                var client = new Generated.SignOperationService.NameOperationServiceClient(channel);

                try
                {
                    var response = client.SayHello(new Generated.SignRequest
                    {
                        Sign = data
                    });

                    Console.WriteLine(response.Message);
                    Thread.Sleep(100);
                }

                catch (Exception e)
                {
                }

            } while (true);

            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
